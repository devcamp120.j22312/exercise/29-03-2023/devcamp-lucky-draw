import { Component } from "react";
import  "../App.css"


class LuckyNumber extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number1: 0,
            number2: 0,
            number3: 0,
            number4: 0,
            number5: 0,
            number6: 0
        }
    }

    luckyNumberHandler  = () => {
        this.setState({
            number1: Math.floor(Math.random() * 100),
            number2: Math.floor(Math.random() * 100),
            number3: Math.floor(Math.random() * 100),
            number4: Math.floor(Math.random() * 100),
            number5: Math.floor(Math.random() * 100),
            number6: Math.floor(Math.random() * 100)
        })
    }

    render() {
        return(
            <div className="container text-center">
                <div>
                    <h1>Lucky Draw</h1>
                </div>
                <div className="row" style={{marginLeft: "300px", marginBottom: "20px", marginTop: "20px"}}>
                    <div style={{width: "50px", height: "50px", backgroundColor: "orange", borderRadius: "50%", marginRight:"10px"}}>
                        <p className="lucky-number">{this.state.number1}</p>
                    </div>
                    <div style={{width: "50px", height: "50px", backgroundColor: "orange", borderRadius: "50%", marginRight:"10px"}}>
                        <p className="lucky-number">{this.state.number2}</p>
                    </div>
                    <div style={{width: "50px", height: "50px", backgroundColor: "orange", borderRadius: "50%", marginRight:"10px"}}>
                        <p className="lucky-number">{this.state.number3}</p>
                    </div>
                    <div style={{width: "50px", height: "50px", backgroundColor: "orange", borderRadius: "50%", marginRight:"10px"}}>
                        <p className="lucky-number">{this.state.number4}</p>
                    </div>
                    <div style={{width: "50px", height: "50px", backgroundColor: "orange", borderRadius: "50%", marginRight:"10px"}}>
                        <p className="lucky-number">{this.state.number5}</p>
                    </div>
                    <div style={{width: "50px", height: "50px", backgroundColor: "orange", borderRadius: "50%", marginRight:"10px"}}>
                        <p className="lucky-number">{this.state.number6}</p>
                    </div>
                </div>
                <div>
                    <button onClick={this.luckyNumberHandler} style={{backgroundColor:"purple", color:"white", width:"100px"}}>Generate</button>
                </div>
            </div>
        )
    }
}
export default LuckyNumber;